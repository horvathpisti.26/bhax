
int* getAlmaPointer(int* almaArray);
typedef int(*A)(int,int);
int getTrueAlmaState(int a, int b);
A getAlmaState(int);

int main()
{

	//egész
	int kosar = 1000;

	//egészre mutató mutató
	int* toKosar = &kosar;

	//C++ feature
	int& refToKosar = kosar;

	//egészek tömbje
	int almaofKosarak[10] = {1,2,3,4,5,6,7,8,9,10};

	//egészek tömbjének referenciája
	int (&refToReactors)[10] = almaofKosarak;

	//egészre mutató mutatók tömbje
	int * pointerArray[10];

	//egészre mutató mutatót visszaadó függvény
	int* myReactor = getAlmaPointer(almaofKosarak);

	//egészre mutató mutatót visszaadó függvényre mutató mutató
	int* (*newpointer)(int*) = getAlmaPointer;

	//egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
	A getTheTrueState = getAlmaState(1000);

	return 0;

}
int* getAlmaPointer(int* reactorArray)
{
	return reactorArray;
}
int getTrueAlmaState(int a, int b)
{
	return -1000;
}

A getAlmaState(int Piros)
{
	return getTrueAlmaState;
}