#include <stdio.h>

int main(){

	int a = 43;
	int b = 258;
	printf("Kiindulási számok: a=%d, b=%d.\n", a, b);

	b = b - a;
	a = a + b;
	b = a - b;
	printf("Csere összeadás és kivonás használatával: a=%d, b=%d.\n", a, b);

	int c = 0;
	c = a;
	a = b;
	b = c;
	printf("Csere segéd változó használatával: a=%d, b=%d.\n", a, b);


	return 0;
}